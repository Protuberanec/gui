#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(&tim, &QTimer::timeout, this, &MainWindow::slot_TimerOut);
    tim.setInterval(700);
    tim.start();

//    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(slot_clickPB1()));
    connect(ui->pushButton, &QPushButton::clicked, this, &MainWindow::slot_clickPB1);
    connect(ui->pushButton, &QPushButton::pressed, this, &MainWindow::slot_pressedPB1);
    connect(ui->pushButton, &QPushButton::released, this, &MainWindow::slot_releasedPB1);
//    connect(ui->pushButton_2, &QPushButton::clicked, this, &MainWindow::slot_clickPB2);
//    connect(ui->pushButton_3, &QPushButton::clicked, this, &MainWindow::slot_clickPB3);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::slot_clickPB2()
{
    ui->pushButton_2->setText("T1");
}

void MainWindow::slot_clickPB3()
{
    ui->label->setText("Hi world... 你好。。。 привет");
}

void MainWindow::slot_clickPB1()
{
    status.push_back(1);
}

void MainWindow::slot_pressedPB1()
{
    status.push_back(2);
}

void MainWindow::slot_releasedPB1()
{
    status.push_back(3);
}

void MainWindow::slot_TimerOut()
{
    static int num = 0;

    if (status.empty()) {
        ui->label->setText("empty");
    }

    if (num >= status.size()) {

        num = 0;
        return;
    }

    ui->label->setText(tr("status %1 : %2").arg(num).arg(status.at(num++)));
}

